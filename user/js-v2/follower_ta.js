(function ($) {
    "use strict";
    var UC = {};
    UC.api = {
        getFollower: "http://fm.qianlong.com/hellofm/m_user/getFocus.do?"
    };
    UC.ele = {
        listFilter: ".filter",
        listFilterBtn: ".filter .toggle-btn",
        listFilterItem: ".filter li",
        listBody: ".list",
        btnFollow: ".btn-follow"
    };
    UC.pageCurrent = 1;
    UC.getFollowerList = function () {
        var url = UC.api.getFollower + "id=8c26120fcf9d45e9bdfd285f048b3b9e&pageSize=10&pageCurrent=" + UC.pageCurrent;
        if ($(UC.ele.listFilterBtn).data("value") !== "") {
            url += "&mutual=" + $(UC.ele.listFilterBtn).data("value");
        }
        $.ajax({
            url: url,
            type: "GET",
            success: function (data) {
                $.each(data.list, function (index, element) {
                    $(UC.ele.listBody).append(
                        '<li>' +
                            '<a href="">' +
                            '<img class="pic" src="http://fm.qianlong.com/' + element.image + '" alt="">' +
                            '</a>' +
                            '<a class="name" href="">' + element.nickname + '</a>' +
                            '<p class="desc">' + element.intro + '</p>' +
                            '<div class="info">' +
                            '声音 : <span>' + element.audio_count + '</span> | 粉丝 : <span>' + element.fans_count + '</span>' +
                            '</div>' +
                            '<div class="btn btn-follow" data-status="0">+ 加关注</div>' +
                            '<a class="btn-mail">发私信</a>' +
                            '<a class="btn-blacklist">加入黑名单</a>' +
                            '<hr>' +
                            '</li>'
                    );
                    //remove myself
                });
            }
        });
    };

    UC.getFollowerList();

    $(UC.ele.listFilter).click(function () {
        $(this).toggleClass("active");
    });
    $(UC.ele.listFilterItem).click(function () {
        $(UC.ele.listFilterBtn).text($(this).text()).data("value", $(this).data("value"));
    });
    $(UC.ele.btnFollow).hover(function () {
        if ($(this).data("status") === 1 || $(this).data("status") === 2) {
            $(this).text("取消关注");
        }
    }, function () {
        if ($(this).data("status") === 0) {
            $(this).text("+ 加关注");
        } else if ($(this).data("status") === 1) {
            $(this).text("已关注");
        } else if ($(this).data("status") === 2) {
            $(this).text("相互关注");
        }
    });
    $(UC.ele.btnFollow).click(function () {
        if ($(this).data("status") === 0) {
            $(this).data("status", 1).addClass("btn-dark").removeClass("btn").text("已关注");
            //or status = 2;
        } else if ($(this).data("status") === 1 || $(this).data("status") === 2) {
            $(this).data("status", 0).removeClass("btn-dark").addClass("btn").text("+ 加关注");
        }
    });
}(window.jQuery));