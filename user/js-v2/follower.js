(function ($) {
    "use strict";
    var UC = {};
    UC.ele = {
        listFilter: ".filter",
        listFilterBtn: ".filter .toggle-btn",
        listFilterItem: ".filter li",
        btnFollow: ".btn-follow"
    };
    $(UC.ele.listFilter).click(function () {
        $(this).toggleClass("active");
    });
    $(UC.ele.listFilterItem).click(function () {
        $(UC.ele.listFilterBtn).text($(this).text()).data("value", $(this).data("value"));
    });
    $(UC.ele.btnFollow).hover(function () {
        if ($(this).data("status") === 2) {
            $(this).text("取消关注");
        }
    }, function () {
        if ($(this).data("status") === 0) {
            $(this).text("+ 加关注");
        } else if ($(this).data("status") === 2) {
            $(this).text("相互关注");
        }
    });
    $(UC.ele.btnFollow).click(function () {
        if ($(this).data("status") === 0) {
            $(this).data("status", 2).addClass("btn-dark").removeClass("btn").text("相互关注");
        } else if ($(this).data("status") === 2) {
            $(this).data("status", 0).removeClass("btn-dark").addClass("btn").text("+ 加关注");
        }
    });
}(window.jQuery));