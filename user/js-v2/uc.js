(function ($) {
    "use strict";
    var UC = {};
    UC.api = {
        getSoundAudio: "http://192.168.42.113:8080/hellofm/m_audio/getPublic.do?",
        getSoundAlbum: "http://192.168.42.113:8080/hellofm/m_album/getPublic.do?",
        getLikeAudio: "http://192.168.42.113:8080/hellofm//m_audio/getLikeAudio.do?"
    };
    UC.ele = {
        userid: "#userid",
        userName: ".upper .info .name",
        itemBar: ".tabBar li",
        itemBox: ".tabContent",
        itemOper: ".operation",
        listSoundAudio: ".soundList .sectionList-audio",
        listSoundAlbum: ".soundList .sectionList-album",
        listLikeAudio: ".likeList .sectionList-audio",
        btnPlay: ".icon-play",
        btnJump: "a",
        btnShare: ".btn-share"
    };
    UC.getSoundAudioList = function () {
        var url = UC.api.getSoundAudio + "pageCurrent=1&pageSize=10&userId=" + $(UC.ele.userid).val();
        $.ajax({
            url: url,
            type: "GET",
            success: function (data) {
                $.each(data.body.list, function (index, element) {
                    $(UC.ele.listSoundAudio).append(
                        '<li>' +
                            '<a href="' + element.home + '" data-id="' + element.id + '">' +
                            '<img class="pic" src="http://fm.qianlong.com/' + element.urls.image + '" alt="">' +
                            '<div class="icon-play"></div>' +
                            '</a>' +
                            '<div class="info-wrapper">' +
                            '<a class="name-audio" href="' + element.home + '" data-id="' + element.id + '">' + element.title + '</a>' +
                            '<br>' +
                            '<a class="name-album" href="' + element.album.album[0].home + '">' + element.album.album[0].title + '</a>' +
                            '<div class="interact">' +
                            '<i class="like"></i><span>' + element.likeCount + '</span>' +
                            '<i class="share"></i><span>' + element.shareCount + '</span>' +
                            '<p>' + element.create_time.substr(0, 10) + '</p>' +
                            '</div>' +
                            '</div>' +
                            '<div class="operation">' +
                            '<div><i></i></div>' +
                            '<ul class="toggle-options">' +
                            '<li class="btn-share" data-title="' + element.title + '" data-url="' + element.home + '" data-img="' + element.urls.image + '">' +
                            '<i class="o-share"></i><span>分享</span>' +
                            '</li>' +
                            '</ul>' +
                            '</div>' +
                            '</li>'
                    );
                });
            }
        });
    };
    UC.getSoundAlbumList = function () {
        var url = UC.api.getSoundAlbum + "pageCurrent=1&pageSize=5&userId=" + $(UC.ele.userid).val();
        $.ajax({
            url: url,
            type: "GET",
            success: function (data) {
                $.each(data.body.list, function (index, element) {
                    $(UC.ele.listSoundAlbum).append(
                        '<li>' +
                            '<a class="pic-wrapper" href="' + element.home + '" data-id="' + element.id + '">' +
                            '<img src="' + element.image + '" alt="">' +
                            '<div class="cover"></div>' +
                            '</a>' +
                            '<a class="title" href="' + element.home + '" data-id="' + element.id + '">' + element.title + '</a>' +
                            '<div class="info">' +
                            '<span><i class="icon-audioCount"></i>' + element.count + '</span > ' +
                            '<span><i class="icon-liked"></i>' + element.likeCount + '</span>' +
                            '</div>' +
                            '</li>'
                    );
                });
            }
        });
    };
    UC.getLikeAudioList = function () {
        var url = UC.api.getLikeAudio + "pageCurrent=1&pageSize=10&userId=" + $(UC.ele.userid).val();
        $.ajax({
            url: url,
            type: "GET",
            success: function (data) {
                $.each(data.body.list, function (index, element) {
                    $(UC.ele.listLikeAudio).append(
                        '<li>' +
                            '<a href="' + element.home + '" data-id="' + element.id + '">' +
                            '<img class="pic" src="http://fm.qianlong.com/' + element.urls.image + '" alt="">' +
                            '<div class="icon-play"></div>' +
                            '</a>' +
                            '<div class="info-wrapper">' +
                            '<a class="name-audio" href="' + element.home + '" data-id="' + element.id + '">' + element.title + '</a>' +
                            '<br>' +
                            '<a class="name-album" href="' + element.album.album[0].home + '">' + element.album.album[0].title + '</a>' +
                            '<div class="interact">' +
                            '<i class="like"></i><span>' + element.likeCount + '</span>' +
                            '<i class="share"></i><span>' + element.shareCount + '</span>' +
                            '<p>' + element.create_time.substr(0, 10) + '</p>' +
                            '</div>' +
                            '</div>' +
                            '<div class="operation">' +
                            '<div><i></i></div>' +
                            '<ul class="toggle-options">' +
                            '<li class="btn-share" data-title="' + element.title + '" data-url="' + element.home + '" data-img="' + element.urls.image + '">' +
                            '<i class="o-share"></i><span>分享</span>' +
                            '</li>' +
                            '</ul>' +
                            '</div>' +
                            '</li>'
                    );
                });
            }
        });
    };
    UC.init = function () {
        UC.getSoundAudioList();
        UC.getSoundAlbumList();
        UC.getLikeAudioList();
    };
    UC.init();

    $(UC.ele.itemBar).click(function () {
        var index = $(this).index(UC.ele.itemBar);
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
        $(UC.ele.itemBox).children().eq(index).addClass("active");
        $(UC.ele.itemBox).children().eq(index).siblings().removeClass("active");
    });
    $(UC.ele.itemBox).on("click", UC.ele.itemOper, function () {
        $(this).toggleClass("active");
    });
    $(UC.ele.itemBox).on("click", UC.ele.btnPlay, function (event) {
        event.preventDefault();
        event.stopPropagation();
        var target = $(this).parent("a"),
            data = {
                opration: "playback",
                src: "UCENTER_HOME",
                list: [{
                    audioid: target.data("id"),
                    title: "",
                    link: "",
                    author: "",
                    home: ""
                }]
            };
        window.console.log(data);
        window.parent.window.apiCtrl.addToListFun(data);
    });
    $(UC.ele.itemBox).on("click", UC.ele.btnJump, function (event) {
        event.preventDefault();
        event.stopPropagation();
        var target = this,
            data = {
                link: target.href,
                src: "UCENTER_HOME"
            };
        window.console.log(data);
        window.parent.window.frame.apiCtrl.jumperFun(data);
    });
    $(UC.ele.itemBox).on("click", UC.ele.btnShare, function () {
        var target = $(this), data;
        data = {
            src: "UCENTER_HOME",
            title: target.data("title"),
            author: $(UC.ele.userName).html(),
            url: "http://fm.qianlong.com" + target.data("url"),
            img: target.data("img")
        };
        window.parent.apiCtrl.doShareFun(data);
    });
}(window.jQuery));