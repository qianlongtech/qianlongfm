(function ($, FM) {
    "use strict";
    var ele = {
        dom: $(window.document),
        imgCaptcha: $("#img-captcha"),
        inputCaptcha: $("#input-captcha"),
        inputEmail: $("#input-email"),
        inputPasswd1: $("#input-password1"),
        inputPasswd2: $("#input-password2"),
        btnSubmit: $("#btn-submit"),
        formGroupInput: $(".form-group input"),
        formGroupP: $(".form-group p")
    };

    function showMessage(i) {
        ele.formGroupP.eq(i).show();
        ele.formGroupInput.eq(i).css("border-color", "#fb2828").focus();
    }

    function checkForm() {
        var flag = true;
        ele.formGroupP.hide();
        ele.formGroupInput.removeAttr("style");
        if (ele.inputPasswd2.val() !== ele.inputPasswd1.val()) {
            showMessage(3);
            flag = false;
        }
        if (ele.inputPasswd1.val().length < 6 || ele.inputPasswd1.val().length > 18) {
            showMessage(2);
            flag = false;
        }
        return flag;
    }

    //initial
    ele.dom.ready(function () {
//        FM.setURL("signup");
        ele.imgCaptcha[0].src = FM.api.captcha;
    });

    //event
    ele.imgCaptcha.click(function (event) {
        this.src = FM.api.captcha + "?" + Math.random();
    });
    ele.formGroupInput.focus(function (event) {
        var obj = $(this).parents(".form-group");
        if (!obj.hasClass("focus")) {
            obj.addClass("focus");
        }
        obj.siblings().removeClass("focus");
    });
    ele.formGroupInput.blur(function (event) {
        var obj = $(this).parents(".form-group");
        obj.removeClass("focus");
    });
    ele.btnSubmit.click(function (event) {
        event.preventDefault();
        if (checkForm()) {
            var form =
                "mail=" + ele.inputEmail.val() +
                "&password=" + ele.inputPasswd1.val() +
                "&code=" + ele.inputCaptcha.val();
            FM.xhr(FM.api.validate, form, function (response0) {
                if (response0.success) {
                    FM.xhr(FM.api.signup, form, function (response) {
                        var data = {};
                        switch (response.code) {
                        case 100:
                            data.msg = FM.lang.signup.e100;
                            showMessage(0);
                            break;
                        case 201:
                            data.msg = FM.lang.signup.e201;
                            showMessage(0);
                            break;
                        case 207:
                            data.msg = FM.lang.signup.e207;
                            showMessage(1);
                            break;
                        }
                        if (response.success) {
                            FM.frame.apiCtrl.jumperFun({
                                link: FM.dir.mail + "?id=" + ele.inputEmail.val()
                            });
                        } else {
                            window.alert(data.msg);
                        }
                    });
                } else {
                    showMessage(0);
                }
            });
            
        }
    });
}(window.jQuery, window.FM));