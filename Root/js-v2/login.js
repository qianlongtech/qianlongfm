(function ($, FM) {
    "use strict";
    var ele = {
        dom: $(window.document),
        inputEmail: $("#input-email"),
        inputPasswd: $("#input-password"),
        btnSubmit: $("#btn-submit"),
        btnJumpWeibo: $(".jump-weibo"),
        btnJumpQQ: $(".jump-qq")
    };
    //init
    ele.dom.ready(function () {
//        FM.setURL("signin");
    });

    //event
    ele.btnSubmit.click(function (event) {
        var form =
            "mail=" + ele.inputEmail.val() +
            "&password=" + ele.inputPasswd.val();
        FM.xhr(FM.api.login, form, function (response) {
            var msg;
            switch (response.code) {
            case 200:
                msg = FM.lang.login.e200;
                break;
            case 202:
                msg = FM.lang.login.e202;
                break;
            case 206:
                msg = FM.lang.login.e206;
                break;
            }
            if (response.success) {
                FM.frame.apiCtrl.checkUserStatus();
                FM.frame.apiCtrl.jumperFun({
                    link: FM.dir.index,
                    src: FM.jump.login
                });
            } else {
                window.alert(msg);
            }
        });
        event.preventDefault();
    });
    ele.btnJumpWeibo.click(function () {
        FM.frame.apiCtrl.accoutnConnect("weibo");
    });
    ele.btnJumpQQ.click(function () {
        FM.frame.apiCtrl.accoutnConnect("qq");
    });
}(window.jQuery, window.FM));