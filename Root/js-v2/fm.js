var FM = {};
(function () {
    "use strict";
    FM.frame = window.parent.window;
    FM.api = {
        captcha: "checkImg.do",
        login: "login.do",
        signup: "signup.do",
        validate: "validateMail.do"
    };
    FM.lang = {
        login: {
            e101: "未知错误",
            e200: "用户名不存在",
            e202: "用户名未认证",
            e206: "密码错误"
        },
        signup: {
            e100: "未知错误",
            e201: "用户已存在",
            e207: "验证码错误"
        }
    };
    FM.dir = {
        index: "indexNew.html",
        login: "login.html",
        mail: "mail.html"
    };
    FM.jump = {
        index: "HOME",
        login: "LOGIN",
        signup: "SIGNUP",
        error: "ERROR",
        album: "ALBUM",
        audio: "AUDIO"
    };
    FM.setURL = function (url) {
        if (FM.checkPlatform() && url === "index") {
            FM.frame.location.href = "http://fm.qianlong.com/indexMobile.html";
        }
        if (FM.frame.PAGE !== "main") {
            location.href = "http://fm.qianlong.com/#type=" + url;
        }
    };
    FM.checkPlatform = function () {
        return (/iPhone|Android|IEMobile|NokiaBrowser/i.test(navigator.userAgent));
    };
    FM.xhr = function (url, param, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", url + "?" + Math.random(), true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send(param);
        xhr.onreadystatechange = function (event) {
            if (xhr.readyState === 4) {
                callback(JSON.parse(xhr.responseText));
            }
        };
    };
}());