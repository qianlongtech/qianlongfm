(function ($, FM) {
    "use strict";
    var ele = {
            dom: $(window.ducument),
            imgSwiper: $(".swiper-slide"),
            boxSwiperPage: $(".swiper-page"),
            imgBanner: $(".banner-img-wrapper a"),
            titleChannel: $(".channel-left-wrapper h1"),
            btnPlay: $(".section-icon"),
            btnJump: $("a")
        },
        imgSwiperIndex = 0;

    //inital
    ele.imgSwiper.each(function () {
        ele.boxSwiperPage.append("<span></span");
    });
    ele.boxSwiperPage.find("span").eq(0).addClass("active");
    ele.imgBanner.eq(0).addClass("active");
    ele.imgBanner.hover(function () {
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
    });
/*
    ele.titleChannel.each(function (i) {
        ele.titleChannel.eq(i)[0].style.backgroundPosition = "0 -" + i * 48 + "px";
    });
*/
    ele.dom.ready(function () {
//        FM.setURL("index");
        ele.boxSwiperPage.find("span").eq(imgSwiperIndex).click();
        setInterval(function () {
            ele.boxSwiperPage.find("span").eq(imgSwiperIndex).click();
            imgSwiperIndex = (imgSwiperIndex >= ele.imgSwiper.length ? 0 : imgSwiperIndex + 1);
        }, 4000);
    });

    //event
    ele.boxSwiperPage.on("click", "span", function () {
        imgSwiperIndex = ele.boxSwiperPage.find("span").index(this);
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
        ele.imgSwiper.eq(imgSwiperIndex).addClass("active");
        ele.imgSwiper.eq(imgSwiperIndex).siblings().removeClass("active");
    });
    ele.btnPlay.on("click", function (event) {
        event.preventDefault();
        event.stopPropagation();
        var target = $(this).parents("a"),
            data = {
                opration: "playback",
                src: FM.jump.index,
                list: [{
                    audioid: target.data("id"),
                    title: "",
                    link: "",
                    author: "",
                    home: ""
                }]
            };
        window.console.log(data);
        FM.frame.apiCtrl.addToListFun(data);
    });
    ele.btnJump.on("click", function (event) {
        event.preventDefault();
        event.stopPropagation();
        var target = this,
            data = {
                link: target.href,
                src: FM.jump.index
            };
        window.console.log(data);
        FM.frame.apiCtrl.jumperFun(data);
    });
}(window.jQuery, window.FM));