(function ($) {
    "use strict";
    var UP = {};
    UP.api = {
        viewImage: "http://192.168.42.113:8080",
        randomTags: "http://192.168.42.113:8080/hellofm/m_tags/randomTags.do",
        listAlbums: "http://192.168.42.113:8080/hellofm/m_album/choiceAlbum.do?pageSize=99999&userId=" + $("input[name=userId]").val(),
        uploadAudio: "http://192.168.42.113:8080/hellofm/uploadAudio.do",
        uploadImage: "http://192.168.42.113:8080/hellofm/uploadImage.do",
        submitForm: "http://192.168.42.113:8080/hellofm/m_audio/addAudio.do"
    };
    UP.ele = {
        audioUploadInput: "#audioupload",
        audioRecomm: "#audio-add-box .recomm",
        audioUpload: "#audioupload.uploadify",
        audioUploadSwf: "#SWFUpload_0",
        audioUploadButton: ".audioupload-button",
        audioUploadQueueItem: "#audioupload-queue .uploadify-queue-item",
        coverUploadInput: "#coverupload",
        coverUpload: "#coverupload.uploadify",
        coverUploadQueueItem: "#coverupload-queue .uploadify-queue-item",
        cropperBox: "#cropper-box",
        cropperImg: "#cropper-box > img",
        cropperButton: "#cropper-button",
        cropperPreview: "#cropper-preview",
        inputAudio: "#input-audio",
        inputAlbum: "#input-album",
        inputAlbumBtn: "#input-album-btn",
        inputAlbumList: "#input-album-list",
        inputAlbumListLi: "#input-album-list li",
        inputAlbumAdd: "#input-album-add",
        iframeWrapper: ".iframe-wrapper",
        iframe: ".iframe",
        inputCover: "#input-cover",
        radio: ".radio",
        inputTags: "#input-tags",
        inputTagsBox: "#input-tags_tagsinput",
        inputTagsAdd: "#input-tags_tag",
        tagsBox: "#tag-box",
        tagsButton: "#tag-button",
        operateBox: ".operate-box",
        form: "form[name=form-audio-add]",
        formSubmitButton: "#form-submit",
        formCancelButton: "#form-cancel",
        formSubmitButtonAl: "#form-submit-al",
        formCancelButtonAl: "#form-cancel-al"
    };
    UP.audioUploadifySetting = {
        "swf": "../../js-v2/uploadify.swf",
        "uploader": UP.api.uploadAudio,
        "buttonClass": "audioupload-button",
        "buttonText": "选择文件",
        "width": 139,
        "height": 42,
        "multi": false,
        "removeCompleted": false,
        "fileTypeExts": "*.mp3",
        "itemTemplate": '<div id="${fileID}" class="uploadify-queue-item">' +
            '<div class="uploadify-progress">' +
            '<div class="uploadify-progress-bar">' +
            '<span class="data"></span>' +
            '</div>' +
            '</div>' +
            '<div class="cancel">' +
            '<a href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')" data-href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')"></a>' +
            '</div>' +
            '<div class="info">' +
            '<span class="status">正在上传：</span>' +
            '<span class="fileName">${fileName} (${fileSize})</span>' +
            '</div>' +
            '</div>',
        "onSelect": function (file) {
            if ($(UP.ele.audioUploadQueueItem).length > 1) {
                $(UP.ele.audioUploadInput).uploadify("cancel");
            }
        },
        "onCancel": function (file) {
            $(UP.ele.audioUpload).removeClass("hideinput");
            $(UP.ele.audioRecomm).show();
            UP.setAudioUploadStep(1);
        },
        "onUploadStart": function (file) {
            $(UP.ele.audioUpload).addClass("hideinput");
            $(UP.ele.audioRecomm).hide();
        },
        "onUploadProgress": function (file, fileBytesLoaded, fileTotalBytes) {
            $('#' + file.id).find('.data').html(Math.round(fileBytesLoaded / fileTotalBytes * 100) + '%');
        },
        "onUploadSuccess": function (file, data) {
            data = $.parseJSON(data);
            $(UP.ele.inputAudio).val(data.message);
            $(UP.ele.audioUpload).removeClass("hideinput");
            $('#' + file.id).find('.status').html('上传成功：');
            $('#' + file.id).find('.data').html('100%');
            UP.setAudioUploadStep(2);
        }
    };
    UP.coverUploadifySetting = {
        "swf": "../../js-v2/uploadify.swf",
        "uploader": UP.api.uploadImage,
        "buttonClass": "coverupload-button",
        "buttonText": "上传图片",
        "width": 84,
        "height": 24,
        "multi": false,
        "removeCompleted": false,
        "fileTypeExts": "*.jpg",
        "itemTemplate": '<div class="input-cover-wrapper uploadify-queue-item" id="${fileID}"><div class="data"></div>' +
            '<div class="cancel">' +
            '<a href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')" data-href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')"></a>' +
            '</div>' +
            '</div>',
        "onSelect": function (file) {
            if ($(UP.ele.coverUploadQueueItem).length > 1) {
                $(UP.ele.coverUploadInput).uploadify("cancel");
            }
        },
        "onCancel": function (file) {
            UP.setCoverUploadStep(1);
        },
        "onUploadStart": function (file) {
            $(UP.ele.coverUpload).addClass("hideinput");
        },
        "onUploadProgress": function (file, fileBytesLoaded, fileTotalBytes) {
            $('#' + file.id).find('.data').html(Math.round(fileBytesLoaded / fileTotalBytes * 100) + '%');
        },
        "onUploadSuccess": function (file, data) {
            data = $.parseJSON(data);
            $(UP.ele.inputCover).val(data.message);
            $(UP.ele.cropperImg).cropper("replace", UP.api.viewImage + data.filename);
            $('#' + file.id).find('.data').html("");
            UP.setCoverUploadStep(2);
        }
    };
    UP.inputTagsSetting = {
        "width": "390px",
        "height": "auto",
        "maxChars": 6,
        "defaultText": "",
        "maxCount": 5
    };
    UP.setAudioUploadStep = function (step) {
        if (step === 1) {
            $(UP.ele.audioUploadQueueItem).find(".cancel a").attr("href", $(this).data("href")).css({
                "background-image": "url('../../img-v2/cancelBig_15x15.png')"
            });
            $(UP.ele.audioUploadButton).addClass("audioupload-button").removeClass("audioupload-button-re").children().html("选择文件");
            $(UP.ele.audioUpload).css({
                "width": "139px",
                "height": "42px",
                "position": "relative",
                "top": "0",
                "left": "0",
                "margin": "0 auto"
            });
            $(UP.ele.audioUploadSwf).attr({
                "width": "139",
                "height": "42"
            });
        }
        if (step === 2) {
            $(UP.ele.audioUploadQueueItem).find(".cancel a").attr("href", "#").css({
                "background-image": "url('../../img-v2/finish_15x15.png')"
            });
            $(UP.ele.audioUploadButton).addClass("audioupload-button-re").removeClass("audioupload-button").children().html("重新上传");
            $(UP.ele.audioUpload).css({
                "width": "89px",
                "height": "24px",
                "position": "absolute",
                "top": "109px",
                "left": "195px",
                "margin": "auto"
            });
            $(UP.ele.audioUploadSwf).attr({
                "width": "89",
                "height": "24"
            });
        }
    };
    UP.setCoverUploadStep = function (step) {
        if (step === 1) {
            $(UP.ele.coverUpload).removeClass("hideinput");
            $(UP.ele.cropperBox).addClass("hide");
            $(UP.ele.cropperPreview).addClass("hide");
            $(UP.ele.coverUploadQueueItem).find(".cancel a").attr("href", $(this).data("href")).css({
                "background-image": "url('../../img-v2/cancelBig_15x15.png')"
            });
        }
        if (step === 2) {
            $(UP.ele.cropperBox).removeClass("hide");
            $(UP.ele.cropperPreview).removeClass("hide");
            $(UP.ele.cropperButton).removeClass("hide");
            $(UP.ele.coverUploadQueueItem).find(".cancel a").attr("href", "#").css({
                "background-image": "url('../../img-v2/finish_15x15.png')"
            });
        }
        if (step === 3) {
            $(UP.ele.cropperBox).addClass("hide");
            $(UP.ele.cropperButton).addClass("hide");
            $(UP.ele.coverUpload).removeClass("hideinput");
        }
    };
    UP.setCropper = function () {
        $(UP.ele.cropperBox).css({
            width: "200px",
            height: "200px"
        }).addClass("hide");
        $(UP.ele.cropperImg).cropper({
            aspectRatio: 1,
            viewMode: 1,
            dragMode: "move",
            checkCrossOrigin: false,
            guides: false,
            autoCropArea: 1,
            cropBoxMovable: false,
            cropBoxResizable: false,
            background: false,
            preview: UP.ele.cropperPreview,
            built: function () {
                $(this).cropper("setCropBoxData", {
                    width: 100,
                    height: 100,
                    top: 50,
                    left: 50
                });
            }
        });
    };
    UP.setAlbums = function () {
        $.ajax({
            url: UP.api.listAlbums,
            type: "GET",
            dataType: "json",
            success: function (data) {
                var html = "";
                $.each(data.body.list, function (index, element) {
                    html += '<li data-id="' + element.id + '">' + element.title + '</li>';
                });
                $(UP.ele.inputAlbumList).html(html);
            }
        });
    };
    UP.setTags = function () {
        $.ajax({
            url: UP.api.randomTags,
            type: "GET",
            dataType: "json",
            success: function (data) {
                var html = "";
                $.each(data, function (index, element) {
                    html += '<span class="tag"><span>' + element.name + '</span></span>';
                });
                $(UP.ele.tagsBox).html(html);
            }
        });
    };
    UP.setIframe = function () {
        $(UP.ele.iframe).load("create_album.html .upload-box", function () {
            UP.dynamicLoading.css("../../css-v2/create_album.css");
            UP.dynamicLoading.js("../../js-v2/create_album.js");
            $(UP.ele.iframe).on("click", UP.ele.formCancelButtonAl, function () {
                $(UP.ele.iframeWrapper).addClass("hide");
            });
            $(UP.ele.iframe).on("click", UP.ele.formSubmitButtonAl, function () {
                $(UP.ele.iframeWrapper).addClass("hide");
            });
        });
    };
    UP.dynamicLoading = {
        css: function (path) {
            if (!path || path.length === 0) {
                throw new Error('argument "path" is required !');
            }
            var head = document.getElementsByTagName("head")[0],
                link = document.createElement("link");
            link.href = path;
            link.rel = "stylesheet";
            link.type = "text/css";
            head.appendChild(link);
        },
        js: function (path) {
            if (!path || path.length === 0) {
                throw new Error('argument "path" is required !');
            }
            var head = document.getElementsByTagName("head")[0],
                script = document.createElement("script");
            script.src = path;
            script.type = "text/javascript";
            head.appendChild(script);
        }
    };
    $(UP.ele.audioUploadInput).uploadify(UP.audioUploadifySetting);
    $(UP.ele.coverUploadInput).uploadify(UP.coverUploadifySetting);
    $(UP.ele.inputTags).tagsInput(UP.inputTagsSetting);
    $(UP.ele.inputAlbumAdd).click(function () {
        $(UP.ele.iframeWrapper).removeClass("hide");
    });
    $(UP.ele.cropperButton).click(function () {
        UP.setCoverUploadStep(3);
        var data = $(UP.ele.cropperImg).cropper("getData", true);
        $("input[name=jcpx]").val(data.x);
        $("input[name=jcpy]").val(data.y);
        $("input[name=jcpw]").val(data.width);
        $("input[name=jcph]").val(data.height);
    });
    $(UP.ele.inputAlbumList).on("click", "li", function () {
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
        $(UP.ele.inputAlbumBtn).html($(this).html());
        $(UP.ele.inputAlbum).val($(this).data("id"));
    });
    $(UP.ele.radio).click(function () {
        if (!$(this).hasClass("checked")) {
            $(this).addClass("checked");
            $(this).siblings().removeClass("checked");
        }
    });
    $(UP.ele.inputTagsAdd).focusin(function () {
        $(UP.ele.inputTagsBox).addClass("onfocus");
    });
    $(UP.ele.inputTagsAdd).focusout(function () {
        $(UP.ele.inputTagsBox).removeClass("onfocus");
    });
    $(UP.ele.tagsButton).click(function () {
        UP.setTags();
    });
    $(UP.ele.tagsBox).on("click", ".tag", function () {
        if ($(UP.ele.inputTagsBox).find(".tag").length < 5) {
            $(UP.ele.inputTags).addTag($(this).text());
        }
    });
    $(UP.ele.formSubmitButton).click(function () {
        $.ajax({
            url: UP.api.submitForm,
            method: "POST",
            data: $(UP.ele.form).serialize(),
            success: function (data) {
                if (data.status === 200) {
                    window.alert("添加成功~");
                }
            }
        });
    });
    UP.setCropper();
    UP.setAlbums();
    UP.setTags();
    UP.setIframe();
}(window.jQuery));