(function ($) {
    "use strict";
    var UP = {};
    UP.api = {
        viewImage: "http://192.168.42.113:8080",
        randomTags: "http://192.168.42.113:8080/hellofm/m_tags/randomTags.do",
        uploadImage: "http://192.168.42.113:8080/hellofm/uploadImage.do",
        submitForm: "http://192.168.42.113:8080/hellofm/m_album/createAlbum.do"
    };
    UP.ele = {
        coverUploadInput: "#coverupload-al",
        coverUpload: "#coverupload-al.uploadify",
        coverUploadQueueItem: "#coverupload-al-queue .uploadify-queue-item",
        cropperBox: "#cropper-box-al",
        cropperImg: "#cropper-box-al > img",
        cropperButton: "#cropper-button-al",
        cropperPreview: "#cropper-preview-al",
        inputCover: "#input-cover-al",
        radio: ".radio",
        inputTags: "#input-tags-al",
        inputTagsBox: "#input-tags-al_tagsinput",
        inputTagsAdd: "#input-tags-al_tag",
        tagsBox: "#tag-box-al",
        tagsButton: "#tag-button-al",
        form: "form[name=form-album-add]",
        formSubmitButton: "#form-submit-al",
        formCancelButton: "#form-cancel-al"
    };
    UP.coverUploadifySetting = {
        "swf": "../../js-v2/uploadify.swf",
        "uploader": UP.api.uploadImage,
        "buttonClass": "coverupload-al-button",
        "buttonText": "上传图片",
        "width": 84,
        "height": 24,
        "multi": false,
        "removeCompleted": false,
        "fileTypeExts": "*.jpg",
        "itemTemplate": '<div class="input-cover-wrapper uploadify-queue-item" id="${fileID}"><div class="data"></div>' +
            '<div class="cancel">' +
            '<a href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')" data-href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')"></a>' +
            '</div>' +
            '</div>',
        "onSelect": function (file) {
            if ($(UP.ele.coverUploadQueueItem).length > 1) {
                $(UP.ele.coverUploadInput).uploadify("cancel");
            }
        },
        "onCancel": function (file) {
            UP.setCoverUploadStep(1);
        },
        "onUploadStart": function (file) {
            $(UP.ele.coverUpload).addClass("hideinput");
        },
        "onUploadProgress": function (file, fileBytesLoaded, fileTotalBytes) {
            $('#' + file.id).find('.data').html(Math.round(fileBytesLoaded / fileTotalBytes * 100) + '%');
        },
        "onUploadSuccess": function (file, data) {
            data = $.parseJSON(data);
            $(UP.ele.inputCover).val(data.message);
            $(UP.ele.cropperImg).cropper("replace", UP.api.viewImage + data.filename);
            $('#' + file.id).find('.data').html("");
            UP.setCoverUploadStep(2);
        }
    };
    UP.inputTagsSetting = {
        "width": "390px",
        "height": "auto",
        "maxChars": 6,
        "defaultText": "",
        "maxCount": 5
    };
    UP.setCoverUploadStep = function (step) {
        if (step === 1) {
            $(UP.ele.coverUpload).removeClass("hideinput");
            $(UP.ele.cropperBox).addClass("hide");
            $(UP.ele.cropperPreview).addClass("hide");
            $(UP.ele.coverUploadQueueItem).find(".cancel a").attr("href", $(this).data("href")).css({
                "background-image": "url('../../img-v2/cancelBig_15x15.png')"
            });
        }
        if (step === 2) {
            $(UP.ele.cropperBox).removeClass("hide");
            $(UP.ele.cropperPreview).removeClass("hide");
            $(UP.ele.cropperButton).removeClass("hide");
            $(UP.ele.coverUploadQueueItem).find(".cancel a").attr("href", "#").css({
                "background-image": "url('../../img-v2/finish_15x15.png')"
            });
        }
        if (step === 3) {
            $(UP.ele.cropperBox).addClass("hide");
            $(UP.ele.cropperButton).addClass("hide");
            $(UP.ele.coverUpload).removeClass("hideinput");
        }
    };
    UP.setCropper = function () {
        $(UP.ele.cropperBox).css({
            width: "200px",
            height: "200px"
        }).addClass("hide");
        $(UP.ele.cropperImg).cropper({
            aspectRatio: 1,
            viewMode: 1,
            dragMode: "move",
            checkCrossOrigin: false,
            guides: false,
            autoCropArea: 1,
            cropBoxMovable: false,
            cropBoxResizable: false,
            background: false,
            preview: UP.ele.cropperPreview,
            built: function () {
                $(this).cropper("setCropBoxData", {
                    width: 100,
                    height: 100,
                    top: 50,
                    left: 50
                });
            }
        });
    };
    UP.setTags = function () {
        $.ajax({
            url: UP.api.randomTags,
            type: "GET",
            dataType: "json",
            success: function (data) {
                var html = "";
                $.each(data, function (index, element) {
                    html += '<span class="tag"><span>' + element.name + '</span></span>';
                });
                $(UP.ele.tagsBox).html(html);
            }
        });
    };
    $(UP.ele.coverUploadInput).uploadify(UP.coverUploadifySetting);
    $(UP.ele.inputTags).tagsInput(UP.inputTagsSetting);
    $(UP.ele.cropperButton).click(function () {
        UP.setCoverUploadStep(3);
        var data = $(UP.ele.cropperImg).cropper("getData", true);
        $("input[name=jcpx]").val(data.x);
        $("input[name=jcpy]").val(data.y);
        $("input[name=jcpw]").val(data.width);
        $("input[name=jcph]").val(data.height);
    });
    $(UP.ele.radio).click(function () {
        if (!$(this).hasClass("checked")) {
            $(this).addClass("checked");
            $(this).siblings().removeClass("checked");
        }
    });
    $(UP.ele.inputTagsAdd).focusin(function () {
        $(UP.ele.inputTagsBox).addClass("onfocus");
    });
    $(UP.ele.inputTagsAdd).focusout(function () {
        $(UP.ele.inputTagsBox).removeClass("onfocus");
    });
    $(UP.ele.tagsButton).click(function () {
        UP.setTags();
    });
    $(UP.ele.tagsBox).on("click", ".tag", function () {
        if ($(UP.ele.inputTagsBox).find(".tag").length < 5) {
            $(UP.ele.inputTags).addTag($(this).text());
        }
    });
    $(UP.ele.formSubmitButton).click(function () {
        $.ajax({
            url: UP.api.submitForm,
            method: "POST",
            data: $(UP.ele.form).serialize(),
            success: function (data) {
                if (data.status === 200) {
                    window.alert("添加成功~");
                }
            }
        });
    });
    UP.setCropper();
    UP.setTags();
}(window.jQuery));